import * as ko from 'knockout'

interface ITodo {
    name : string,
    ischecked : boolean
}

const todos : KnockoutObservableArray<ITodo> = ko.observableArray([]);
const todonbr : KnockoutComputed<number> = ko.pureComputed(() => todos().length);

todos.push({ name : "make dinner", ischecked : false });

todos.subscribe((newvale) => {
    console.log("todos updated")
    newvale.forEach((val) => {
        console.log(JSON.stringify(val));
    });
});

todonbr.subscribe((newvale) => {
    console.log("todonbr updated")
});

todos.push({ name : "do the shopping", ischecked : false });

const tododata = todos();
tododata[0].ischecked = false;

todos(tododata);