import * as ko from 'knockout'

interface ITodo {
    name : string,
    ischecked : KnockoutObservable<boolean>
}

function makeTodo(name : string) : ITodo {
    return { name : name, ischecked : ko.observable(false) };
}

// state
const items : KnockoutObservableArray<ITodo> = ko.observableArray([]);
items.push(makeTodo("make dinner"));
items.push(makeTodo("do shopping"));

// computed properties
const nbr = ko.pureComputed(() => items().length);
const checkeditems = ko.pureComputed(() => items().filter((v) => v.ischecked()));
const uncheckeditems = ko.pureComputed(() => items().filter((v) => !v.ischecked()));
const nbrchecked = ko.pureComputed(() => checkeditems().length);

// actions
const newTodoName = ko.observable("");

function addItem() : void {
    if (!newTodoName()) { return }
    items.push(makeTodo(newTodoName()));
}

function clear() {
    items(uncheckeditems());
}

const viewmodel = { items, nbr, nbrchecked, newTodoName, addItem, clear };

document.addEventListener("DOMContentLoaded", function(event) { 
    ko.applyBindings(viewmodel, document.getElementById("todoroot"));
});
