/* global require module process __dirname */
/* eslint no-undef: 'error' */
const path = require('path');
const webpack = require('webpack');

module.exports = {
    resolve: {
        extensions: ['.ts']
    },

    entry: {
        'index': './src/index'
    },

    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist')
    },

    module: {
        loaders: [
            {
                test: /\.ts$/,
                loader: 'ts-loader',
                include: path.join(__dirname, 'src'),
            }
        ]
    },
};