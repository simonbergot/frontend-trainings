import * as React from "react";
import * as ReactDOM from "react-dom";

interface HelloProps { compiler: string; framework: string; }

// 'HelloProps' describes the shape of props.
// State is never set so we use the 'undefined' type.
class Hello extends React.Component<HelloProps, any> {
    render() {
        return <h1>Hello here from {this.props.compiler} and {this.props.framework}!</h1>;
    }
}

// function HelloPure(props: HelloProps) {
//     return <h1>
//         Hello {'pure'} from {props.compiler} and {props.framework}!
//         {
//             ["fred", "mahdi"].map(n => <p>{n}</p>)
//         }
//     </h1>;
// }


ReactDOM.render(
    <Hello compiler="TypeScript" framework="React" />,
    document.getElementById("example")
);