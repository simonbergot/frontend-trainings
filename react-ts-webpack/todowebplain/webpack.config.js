/* global require module process __dirname */
/* eslint no-undef: 'error' */
const path = require('path');
const webpack = require('webpack');

module.exports = {
    resolve: {
        extensions: ['.ts', '.tsx']
    },

    entry: {
        'index': './src/index'
    },

    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist')
    },

    module: {
        rules: [
            { test: /\.tsx?$/, loader: 'ts-loader' }
        ]
    },

    externals: {
        "react": "React",
        "react-dom": "ReactDOM"
    },
};