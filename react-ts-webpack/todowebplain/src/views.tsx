import * as React from "react";
import * as ReactDOM from "react-dom";

interface TodoInputProps {
    addTodo : (s: string) => void;
}

interface TodoInputState {
    newTodo : string;
}

interface TodoEntriesProps {
    todos: string[];
    removeTodo: (i: number) => void;
}

export class TodoInput extends React.Component<TodoInputProps, TodoInputState> {
    constructor() {
        super();
        this.state = { newTodo: "" };
    }
    render() {
        return <div>
            <input
                onChange={(e) => this.setState({newTodo : e.target.value})}
                value={this.state.newTodo}>
            </input>
            <button onClick={() => this.props.addTodo(this.state.newTodo)} >add</button>
        </div>;
    }
}

export class TodoEntries extends React.Component<TodoEntriesProps, {}> {
    render() {
        const style = {
            margin: "5px"
        };
        return <ul>
                {this.props.todos.map((t, i) => {
                    return <li key={i}>
                        <span style={style}>{i} {t}</span>
                        <button onClick={() => this.props.removeTodo(i)}>remove</button>
                    </li>;
                })}
            </ul>
    }
}