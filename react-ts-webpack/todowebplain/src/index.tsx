import * as React from "react";
import * as ReactDOM from "react-dom";

import * as Views from "./views";

interface ContainerProps {}
interface ContainerState {
    todos: string[];
}

// 'HelloProps' describes the shape of props.
// State is never set so we use the 'undefined' type.
class Container extends React.Component<ContainerProps, ContainerState> {
    /**
     *
     */
    constructor() {
        super();
        this.state = {
            todos : ["do laundry", "show react"]
        };
    }

    removeTodo(i: number) {
        const todos = this.state.todos;
        todos.splice(i, 1);
        this.setState({ todos })
    }

    addTodo(newTodo: string) {
        if (!newTodo) { return; }
        const todos = this.state.todos;
        todos.push(newTodo);
        this.setState({ todos })
    }

    render() {
        return <div>
            <h1>
                your task list
            </h1>
            <p>number of todo: {this.state.todos.length}</p>
            <Views.TodoInput addTodo={(newTodo) => this.addTodo(newTodo)} />
            <Views.TodoEntries todos={this.state.todos} removeTodo={(i) => this.removeTodo(i)} />
        </div>;
    }
}

ReactDOM.render(
    <Container />,
    document.getElementById("example")
);