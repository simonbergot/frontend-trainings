import * as React from "react";
import * as ReactDOM from "react-dom";

interface TodoProps { compiler: string; framework: string; }
interface TodoState {
    newTodo: string;
    todos: string[];
}

// 'HelloProps' describes the shape of props.
// State is never set so we use the 'undefined' type.
class Todo extends React.Component<TodoProps, TodoState> {
    /**
     *
     */
    constructor() {
        super();
        this.state = {
            newTodo: "",
            todos : ["do laundry", "show react"]
        };
    }

    removeTodo(i: number) {
        const todos = this.state.todos;
        todos.splice(i, 1);
        this.setState({ todos })
    }

    addTodo() {
        const todos = this.state.todos;
        todos.push(this.state.newTodo);
        this.setState({ todos : todos })       
    }

    render() {
        const style = {
            margin: "5px"
        };
        return <div>
            <h1 className="titi" >
                your task list
            </h1>
            <input onChange={(event) => this.setState({newTodo: event.target.value})} value={this.state.newTodo}></input>
            {/*<input value="tata"></input>*/}
            <button onClick={() => this.addTodo()}>add</button>
            <ul>
                {this.state.todos.map((t, i) => {
                    return <li>
                        <span style={style}>{i} {t}</span>
                        <button onClick={() => this.removeTodo(i)}>remove</button>
                    </li>;
                })}
            </ul>
        </div>;
    }
}

ReactDOM.render(
    <Todo compiler="TypeScript titi" framework="React" />,
    document.getElementById("example")
);