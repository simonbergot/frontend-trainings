import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider, Dispatch } from 'react-redux'
import { createStore, applyMiddleware, Store, Action, Middleware, MiddlewareAPI } from "redux";
import { updateState } from "./reducers";

import { Todo } from "./components/Todo";

// redux offer a middleware abstraction similar to the owin/asp.net core one
// here we create a middleware that will allow us to log every state change
const logger = ((api: MiddlewareAPI<AppState>) => (next: Dispatch<AppState>) => (action: Action) => {
  console.log('old state', store.getState())
  console.log('dispatching', action)
  // we could add a try catch to implement crash report
  let result = next(action)
  console.log('next state', store.getState())
  return result
})  as Middleware

// the store save the state and call the reducers
const store = createStore(updateState, applyMiddleware(logger));

ReactDOM.render(
    // the Provider helper magically wire the store to all components
    <Provider store={store} >
        <Todo />
    </Provider>,
    document.getElementById("example")
);