import { combineReducers } from "redux";

function todosReducer(state: string[] | undefined, action: TodoAction): string[] {
    // the first redux call is with an undefined state
    if (typeof state === 'undefined') {
        return [];
    }

    switch (action.type) {
        case "ADD": {
            // action is AddTodoAction for typescript
            const todos = state.slice();
            const newTodo = action.newTodo;
            if (newTodo !== "") {
                todos.push(newTodo);
            }
            return todos;
        }
        case "REMOVE": {
            const todos = state.splice(action.idx);
            return todos;
        }
        default: {
            return state;
        }
    }
}

function newTodoReducer(state: string | undefined, action: UpdateNewTodoAction): string {
    // the first redux call is with an undefined state
    if (typeof state === 'undefined') {
        return "";
    }

    switch (action.type) {
        case "UPDATE": {
            return action.newTodo
        }
        default: {
            return state;
        }
    }
}

export const updateState = combineReducers<AppState>({
    todos: todosReducer,
    newTodo: newTodoReducer
});

// we could log like this but it is better to use a middleware

// export function handleWithlog(state: AppState, action: Action): AppState {
//     const oldstate = state;
//     const newstate = updateState(state, action);
//     console.log({ action, oldstate, newstate });
//     return newstate;
// }