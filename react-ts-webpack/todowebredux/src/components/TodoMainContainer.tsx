import { Dispatch } from "redux";
import { connect } from "react-redux";
import * as Actions from "../actions";
import { TodoUI } from "./TodoUI";

function mapStateToProps(state: AppState): TodoPropsData {
    return { todos: state.todos };
}

function mapDispatchToProps(dispatch: Dispatch<AppState>): TodoPropsActions {
    return {
        addTodo: (newTodo: string) => dispatch(Actions.addTodo(newTodo)),
        removeTodo: (i: number) => dispatch(Actions.removeTodo(i))
    }
}

export const TodoMainContainer = connect(mapStateToProps, mapDispatchToProps)(TodoUI);