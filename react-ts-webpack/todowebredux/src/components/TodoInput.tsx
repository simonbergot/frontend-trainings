import * as React from "react";

// we can use redux state or react state here depending on what we want do
// see: https://stackoverflow.com/questions/34952530/i-am-using-redux-should-i-manage-controlled-input-state-in-the-redux-store-or-u

// here, the redux state is used. This is way overkill and we should use a library such as http://redux-form.com/6.7.0/docs/GettingStarted.md/ 
// if we really want to put the form state in the redux state

export class TodoInput extends React.Component<TodoInputProps, {}> {
    render() {
        return <div>
            <input
                onChange={(e) => this.props.updateNewTodo(e.target.value)}
                value={this.props.newTodo}>
            </input>
            <button onClick={() => this.props.addTodo(this.props.newTodo)} >
                add
            </button>
        </div>;
    }
}