import * as React from "react";
import { TodoInputContainer } from "./TodoInputContainer";
import { TodoEntries } from "./TodoEntries";

export class TodoUI extends React.Component<TodoProps, {}> {
    render() {
        return <div>
            <TodoInputContainer />
            <TodoEntries todos={this.props.todos} removeTodo={this.props.removeTodo} />
        </div>;
    }
}