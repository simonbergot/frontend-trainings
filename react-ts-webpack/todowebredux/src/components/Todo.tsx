import * as React from "react";

import { TodoMainContainer } from "./TodoMainContainer"
import { TodoHeaderContainer } from "./TodoHeaderContainer"

export function Todo() {
    return <div>
        <TodoHeaderContainer />
        <TodoMainContainer />
    </div>;
}