import * as React from "react";

interface TodoEntriesProps {
    todos: string[];
    removeTodo: (i: number) => void;
}

export class TodoEntries extends React.Component<TodoEntriesProps, {}> {
    render() {
        const style = {
            margin: "5px"
        };
        return <ul>
                {this.props.todos.map((t, i) => {
                    return <li key={i}>
                        <span style={style}>{i} {t}</span>
                        <button onClick={() => this.props.removeTodo(i)}>remove</button>
                    </li>;
                })}
            </ul>
    }
}