import * as React from "react";
import * as ReactDOM from "react-dom";


export function TodoHeader({ todocount } : TodoHeaderProps) {
    return <div>
            <h1>
                your task list
            </h1>
            <p>number of todo: {todocount}</p>
        </div>
}