import { Dispatch } from "redux";
import { connect } from "react-redux";
import * as Actions from "../actions";
import { TodoInput } from "./TodoInput";

function mapStateToProps(state: AppState): TodoInputPropsData {
    return { newTodo: state.newTodo };
}

function mapDispatchToProps(dispatch: Dispatch<AppState>): TodoInputPropsActions {
    return {
        addTodo: (newTodo: string) => dispatch(Actions.addTodo(newTodo)),
        updateNewTodo: (text: string) => dispatch(Actions.updateNewTodo(text))
    };
}

export const TodoInputContainer = connect(mapStateToProps, mapDispatchToProps)(TodoInput);