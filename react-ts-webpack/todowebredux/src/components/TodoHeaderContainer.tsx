import { connect } from "react-redux";
import { TodoHeader } from "./TodoHeader";

function mapStateToProps(state: AppState): TodoHeaderProps {
    return {
        todocount: state.todos.length
    };
}

export const TodoHeaderContainer = connect(mapStateToProps)(TodoHeader);