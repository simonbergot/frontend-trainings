interface AppState {
    todos: string[];
    newTodo: string;
}

// we have to split the interfaces to be able
// to correctly typecheck the redux mapping functions

interface TodoPropsData {
    todos: string[];
}

interface TodoPropsActions {
    addTodo: (todo: string) => void;
    removeTodo: (i: number) => void;
}

type TodoProps = TodoPropsData & TodoPropsActions;

interface TodoHeaderProps {
    todocount: number;
}

interface TodoInputPropsActions {
    addTodo: (newTodo: string) => void;
    updateNewTodo: (text: string) => void;
}

interface TodoInputPropsData {
    newTodo: string;
}

type TodoInputProps = TodoInputPropsActions & TodoInputPropsData;

// typescript tagged union to represent actions
// checking the type property with a static value will allow
// typescript to narrow the action type (see the handle function)

// https://blog.mariusschulz.com/2016/11/03/typescript-2-0-tagged-union-types

interface AddTodoAction {
    type: "ADD";
    newTodo: string;
}

interface RemoveTodoAction {
    type: "REMOVE";
    idx: number;
}

type TodoAction = AddTodoAction | RemoveTodoAction;

interface UpdateNewTodoAction {
    type: "UPDATE";
    newTodo: string;
}

type Action = TodoAction | UpdateNewTodoAction;
