export function addTodo(newTodo: string): AddTodoAction {
    return {
        type: "ADD",
        newTodo
    };
}

export function removeTodo(idx: number): RemoveTodoAction {
    return {
        type: "REMOVE",
        idx
    };
}

export function updateNewTodo(text: string): UpdateNewTodoAction {
    return {
        type: "UPDATE",
        newTodo: text
    }
}