import * as React from "react";
import * as ReactDOM from "react-dom";
import { MyJiraClient } from './data';

interface IPongProps { response: string };
interface IPongState { };

class Ping extends React.Component<IPongProps, IPongState> {
    render() {
        return <h1>Ping? {this.props.response}</h1>;
    }
}

const client = new MyJiraClient("http://10.100.0.170:5000");
client.ping().then((r) => {
    ReactDOM.render(
        <Ping response={r} />,
        document.getElementById("example")
    );
});

