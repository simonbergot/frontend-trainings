import { TextDecoder } from 'text-encoding';

export interface ITicket
{
    author: string;
    title: string;
    status: Status;
    rank: number;
}

export interface ITicketWithId extends ITicket
{
    id: number;
}

export type Status = "todo" | "active" | "review" | "done";

export class MyJiraClient
{
    url: string;

    constructor(url: string)
    {
        this.url = url;
    }

    async ping(): Promise<string>
    {
        const response = await fetch(`${this.url}/ping`);
        const body = response.body;
        if (!body)
        {
            return ""
        }
        const raw = await body.getReader().read();
        const enc = new TextDecoder();
        return enc.decode(raw.value);
    }

    async getTickets(): Promise<ITicketWithId[]>
    {
        const ticketsraw = await fetch(`${this.url}/ticket`);
        const ticket = await ticketsraw.json() as ITicketWithId[];
        return ticket;
    }

    async getTicketById(id: number): Promise<ITicketWithId>
    {
        const ticketsraw = await fetch(`${this.url}/ticket/${id}`);
        const ticket = await ticketsraw.json() as ITicketWithId;
        return ticket;
    }

    async searchTickets(partialTicket: Partial<ITicket>): Promise<ITicketWithId[]>
    {
        const ticketsraw = await fetch(`${this.url}/searchticket?${toQueryString(partialTicket)}`);
        const tickets = await ticketsraw.json() as ITicketWithId[];
        return tickets;
    }

    async createTicket(ticket: ITicket): Promise<ITicketWithId>
    {
        const form = new FormData();
        form.append("author", ticket.author);
        form.append("title", ticket.title);
        form.append("status", ticket.status);
        form.append("rank", ticket.rank.toString());
        const ticketsraw = await fetch(
            `${this.url}/ticket`,
            {
                method: "POST",
                body: form
            });
        const createdTicket = await ticketsraw.json() as ITicketWithId;
        return createdTicket;
    }

    async updateTicket(ticket: ITicketWithId): Promise<ITicketWithId>
    {
        const form = new FormData();
        form.append("author", ticket.author);
        form.append("title", ticket.title);
        form.append("status", ticket.status);
        const ticketsraw = await fetch(
            `${this.url}/ticket/${ticket.id}`,
            {
                method: "PUT",
                body: form
            });
        const createdTicket = await ticketsraw.json() as ITicketWithId;
        return createdTicket;
    }
}

function toQueryString(obj: any) {
    var str = [];
    for(var p in obj) {
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
    }
    return str.join("&");
}