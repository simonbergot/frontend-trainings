import sys
sys.path.append('./vendor')

from flask import Flask, request, jsonify
from flask_cors import CORS
from tinydb import TinyDB, Query
from functools import reduce

app = Flask(__name__)
CORS(app)
db = TinyDB('./db.json')

def validate_status(status):
    if status not in STATUS_LIST:
        raise InvalidUsage({"invalid_status" : status})
    return status

TICKET_MODEL = {
    ("title", str),
    ("author", str),
    ("status", validate_status),
    ("rank", int)
    }
TICKET_FIELDS = set(field for field, _ in TICKET_MODEL)
STATUS_LIST = {"todo", "active", "review", "done"}

@app.route('/ping')
def hello_world():
    return 'Pong!'

@app.route('/ticket', methods=['GET', 'POST'])
def ticket():
    if request.method == 'POST':
        return jsonify(insert_ticket(request.form)), 200
    if request.method == 'GET':
        tickets = db.all()
        for t in tickets:
            t["id"] = t.doc_id
        return jsonify(tickets), 200

@app.route('/ticket/<int:ticket_id>', methods=['GET', 'PUT'])
def ticket_by_id(ticket_id):
    if not db.contains(doc_ids=[ticket_id]):
        raise NotFound({"error" : "not found"})
    if request.method == 'PUT':
        return jsonify(update_ticket(request.form, ticket_id)), 200
    if request.method == 'GET':
        ticket = db.get(doc_id=ticket_id)
        ticket["id"] = ticket.doc_id
        return jsonify(ticket), 200

@app.route('/searchticket', methods=['GET'])
def search_ticket():
    check_invalid_fields(request.args)
    valid_fields = get_all_valid_fields(request.args)

    tickets = []
    if not valid_fields:
        tickets = db.all()
    else:
        Ticket = Query()
        all_conds = [Ticket[field] == ftype(request.args[field]) for field, ftype in valid_fields]
        total_cond = reduce(lambda a, b: a & b, all_conds)
        tickets = db.search(total_cond)

    for t in tickets:
        t["id"] = t.doc_id
    return jsonify(tickets), 200

def check_invalid_fields(src):
    invalid_fields = [field for field in src if field not in TICKET_FIELDS]
    if invalid_fields:
        raise InvalidUsage({"invalid_fields" : invalid_fields})

def get_all_valid_fields(src):
    return [(field, ftype) for field, ftype in TICKET_MODEL if field in src]

def validate_ticket(form):
    missing = [field for field in TICKET_FIELDS if field not in form]
    if missing:
        raise InvalidUsage({"missing_fields" : missing})

def make_ticket(form):
    validate_ticket(form)
    ticket = {}
    for field, ftype in TICKET_MODEL:
        ticket[field] = ftype(form[field])
    return ticket

def insert_ticket(form):
    ticket = make_ticket(form)
    id = db.insert(ticket)
    ticket["id"] = id
    return ticket

def update_ticket(form, id):
    ticket = make_ticket(form)
    db.update(ticket, doc_ids=[id])
    ticket["id"] = id
    return ticket

class InvalidUsage(Exception):
    status_code = 400

    def __init__(self, message):
        Exception.__init__(self)
        self.message = message

    def to_dict(self):
        return self.message

class NotFound(Exception):
    status_code = 404

    def __init__(self, message):
        Exception.__init__(self)
        self.message = message

    def to_dict(self):
        return self.message

@app.errorhandler(InvalidUsage)
@app.errorhandler(NotFound)
def handle_invalid_usage(error):
    print(error.to_dict())
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response
