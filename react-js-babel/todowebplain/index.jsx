class Container extends React.Component {
    /**
     *
     */
    constructor() {
        super();
        this.state = {
            todos : ["do laundry", "show react"]
        };
    }

    removeTodo(i) {
        const todos = this.state.todos;
        todos.splice(i, 1);
        this.setState({ todos })
    }

    addTodo(newTodo) {
        if (!newTodo) { return; }
        const todos = [...this.state.todos];
        todos.push(newTodo);
        this.setState({ todos })
    }

    render() {
        return <div>
            <h1>
                your task list
            </h1>
            <p>number of todo: {this.state.todos.length}</p>
            <TodoInput addTodo={(newTodo) => this.addTodo(newTodo)} />
            <TodoEntries todos={this.state.todos} removeTodo={(i) => this.removeTodo(i)} />
        </div>;
    }
}

class TodoInput extends React.Component {
    constructor() {
        super();
        this.state = { newTodo: "" };
    }
    render() {
        return <div>
            <input
                onChange={(e) => this.setState({newTodo : e.target.value})}
                value={this.state.newTodo}>
            </input>
            <button onClick={() => this.props.addTodo(this.state.newTodo)} >add</button>
        </div>;
    }
}

class TodoEntries extends React.Component {
    render() {
        const style = {
            margin: "5px"
        };
        return <ul>
                {this.props.todos.map((t, i) => {
                    return <li key={i}>
                        <span style={style}>{i} {t}</span>
                        <button onClick={() => this.props.removeTodo(i)}>remove</button>
                    </li>;
                })}
            </ul>
    }
}

ReactDOM.render(
    <Container />,
    document.getElementById("root")
);