Requirements:
- git (to clone this repo)
- frontend code editor (vscode)
- node/yarn
- npx

Setup:
- go to https://bitbucket.org/simonbergot/frontend-trainings and clone the repo in a new folder
- open react-js-babel/myjira/client with your editor
- run "yarn global add npx" anywhere
- run "npx serve" in myjira\client
- edit the index.jsx file and use the 10.20.20.98 ip address


The browser should display the text "Ping? Pong!".

Build a web client that manage the following features:

1.  show the first ticket
2.  list all tickets
3.  add a button that creates a hardcoded ticket (refresh page to show new ticket)
4.  add a text input that allow the user to set the title of the new ticket
5.  change the code so that the ticket list is automatically refreshed
6.  allow the user to set the values for the author, status and rank properties
7.  allow the user to edit a ticket
8.  allow the user to search tickets (check the client api)
9.  provide a view where tickets are grouped into columns by status
10. allow the user to change the status or the rank of a ticket with a drag & drop operation
