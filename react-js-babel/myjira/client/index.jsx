class Ping extends React.Component {
    render() {
        return <h1>Ping? {this.props.response}</h1>;
    }
}

const client = new MyJiraClient("http://192.168.43.159:5001");
client.ping().then((r) => {
    ReactDOM.render(
        <Ping response={r} />,
        document.getElementById("root")
    );
});

