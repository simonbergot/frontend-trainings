class MyJiraClient
{
    url;

    constructor(url)
    {
        this.url = url;
    }

    async ping()
    {
        const response = await fetch(`${this.url}/ping`);
        return await response.text();
    }

    async getTickets()
    {
        const ticketsraw = await fetch(`${this.url}/ticket`);
        const ticket = await ticketsraw.json();
        return ticket;
    }

    async getTicketById(id)
    {
        const ticketsraw = await fetch(`${this.url}/ticket/${id}`);
        const ticket = await ticketsraw.json();
        return ticket;
    }

    async searchTickets(partialTicket)
    {
        const ticketsraw = await fetch(`${this.url}/searchticket?${toQueryString(partialTicket)}`);
        const tickets = await ticketsraw.json();
        return tickets;
    }

    async createTicket(ticket)
    {
        const form = new FormData();
        form.append("author", ticket.author);
        form.append("title", ticket.title);
        form.append("status", ticket.status);
        form.append("rank", ticket.rank.toString());
        const ticketsraw = await fetch(
            `${this.url}/ticket`,
            {
                method: "POST",
                body: form
            });
        const createdTicket = await ticketsraw.json();
        return createdTicket;
    }

    async updateTicket(ticket)
    {
        const form = new FormData();
        form.append("author", ticket.author);
        form.append("title", ticket.title);
        form.append("status", ticket.status);
        const ticketsraw = await fetch(
            `${this.url}/ticket/${ticket.id}`,
            {
                method: "PUT",
                body: form
            });
        const createdTicket = await ticketsraw.json();
        return createdTicket;
    }
}

function toQueryString(obj) {
    var str = [];
    for(var p in obj) {
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
    }
    return str.join("&");
}

window.MyJiraClient = MyJiraClient;