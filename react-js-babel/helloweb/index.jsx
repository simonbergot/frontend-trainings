// class Hello extends React.Component {
//     render() {
//         return <h1>Hello here from {this.props.compiler} and {this.props.framework}!</h1>;
//     }
// }

function HelloPure(props) {
    return <h1>
        Hello pure {1 + 1} from {props.compiler} and {props.framework}!
        {
            ["brett", "jamie"].map(n => <p>{n}</p>)
        }
        { 1 > 0 ? "true" : "false" }
    </h1>;
}


ReactDOM.render(
    <HelloPure compiler="Babel" framework="React" />,
    document.getElementById("root")
);