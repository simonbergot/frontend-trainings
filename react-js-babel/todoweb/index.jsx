class Todo extends React.Component {
    constructor() {
        super();
        this.state = {
            newTodo: "",
            todos : ["do laundry", "show react"]
        };
    }

    removeTodo(i) {
        const todos = [...this.state.todos];
        todos.splice(i, 1);
        this.setState({ todos: todos });
    }

    addTodo() {
        const todos = this.state.todos;
        todos.push(this.state.newTodo);
        this.setState({ todos : todos })
    }

    render() {
        const style = {
            margin: "5px"
        };
        console.log("render");
        return <div>
            <h1 className="titi" >
                your task list
            </h1>
            <input
                onChange={(event) => {this.setState({newTodo: event.target.value})}}
                value={this.state.newTodo} />
            {/* <input></input> */}
            <button onClick={() => this.addTodo()}>add</button>
            <ul>
                {this.state.todos.map((t, i) => {
                    return <li>
                        <span style={style}>{i} {t}</span>
                        <button onClick={() => this.removeTodo(i)}>remove</button>
                    </li>;
                })}
            </ul>
        </div>;
    }
}

ReactDOM.render(
    <Todo compiler="TypeScript" framework="React" />,
    document.getElementById("root")
);